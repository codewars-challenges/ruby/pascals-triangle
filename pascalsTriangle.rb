def pascalsTriangle(n)
  (0..(n - 1)).to_a.each.map { |i| row(i) }.flatten
end

def row(n)
  (0..n).to_a.each.map { |i| fac(n) / (fac(i) * fac(n - i)) }
end

def fac(n)
  n.zero? ? 1 : (1..n).reduce(1, :*)
end

# tests
1.upto(15) { |n| puts "#{n} => #{pascalsTriangle(n).join(",")}" }
